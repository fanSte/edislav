# Edislav
Goal of this project is to rewrite https://viewsourcecode.org/snaptoken/kilo/
editor using modern C++.

# Dependencies
Project use Bazel as build and test tool. Installation steps can be found at: https://docs.bazel.build/versions/3.7.0/install.html

# Build, run and test
### Build all
    bazel build ...

### Run edislav editor
    bazel run //src/edislav/...
    bazel run //src/edislav/... ~/edislav/.gitignore

### Run all unit tests
    bazel test ...

# NOTE
There is no unit test added, just infrastructure.

At this moment editor supports:

* File opening passed as program argument
* Cursor move with *arrows* and *PgUp, PgDown, Home and End*
* Exit on *ctrl+q*
