#include "src/lib/editor.h"

int main(int argc, char *argv[])
{
  Editor edislav{argc, argv};

  edislav.Run();

  return 0;
}
