#include "src/lib/terminal.h"
#include "src/lib/character.h"
#include <array>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>

const char kTerminalEsc{'\x1b'};
const std::string kTerminalEscSequence{std::string{kTerminalEsc} + "["};
const std::string kTerminalClearScreen{kTerminalEscSequence + "2J"};
const std::string kTerminalCursorAtBegin{kTerminalEscSequence + "H"};
const std::string kTerminalHideCursor{kTerminalEscSequence + "?25l"};
const std::string kTerminalShowCursor{kTerminalEscSequence + "?25h"};
const std::string kTerminalCursorToRightColumn{kTerminalEscSequence + "999C"};
const std::string kTerminalCursorToBottomRow{kTerminalEscSequence + "999B"};
const std::string kTerminalQueryCursorPosition{kTerminalEscSequence + "6n"};

Terminal::Terminal()
{
  SaveTerminal();
  EnableRawMode();
  GetWindowSize();
}

Terminal::~Terminal()
{
  ClearScreen();
  RestoreTerminal();
}

Terminal &Terminal::GetInstance()
{
  static Terminal raw_terminal{};
  return raw_terminal;
}

void Terminal::RestoreTerminal()
{
  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &original_termios_) == -1)
  {
    Die("tcsetattr");
  }
}

void Terminal::SaveTerminal()
{
  if (tcgetattr(STDIN_FILENO, &original_termios_) == -1)
  {
    Die("tcgetattr");
  }
}

void Terminal::EnableRawMode()
{
  raw_termios_ = original_termios_;
  raw_termios_.c_iflag &= ~(ICRNL | IXON);
  raw_termios_.c_oflag &= ~(OPOST);
  raw_termios_.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
  raw_termios_.c_cc[VMIN] = 0;
  raw_termios_.c_cc[VTIME] = 1;

  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw_termios_) == -1)
  {
    Die("tcsetattr");
  }
}

std::int32_t Terminal::ReadChar()
{
  std::int32_t nread;
  char character;
  while ((nread = read(STDIN_FILENO, &character, 1)) != 1)
  {
    if (nread == -1 && errno != EAGAIN)
    {
      Die("read");
    }
  }

  if (character == kTerminalEsc)
  {
    char seq[3];
    if (read(STDIN_FILENO, &seq[0], 1) != 1)
    {
      return kTerminalEsc;
    }
    if (read(STDIN_FILENO, &seq[1], 1) != 1)
    {
      return kTerminalEsc;
    }

    if (seq[0] == '[')
    {
      if (seq[1] >= '0' && seq[1] <= '9')
      {
        if (read(STDIN_FILENO, &seq[2], 1) != 1)
          return kTerminalEsc;
        if (seq[2] == '~')
        {
          switch (seq[1])
          {
          case '1':
            return kSpecialKeys::kHome;
          case '3':
            return kSpecialKeys::kDelete;
          case '4':
            return kSpecialKeys::kEnd;
          case '5':
            return kSpecialKeys::kPageUp;
          case '6':
            return kSpecialKeys::kPageDown;
          case '7':
            return kSpecialKeys::kHome;
          case '8':
            return kSpecialKeys::kEnd;
          }
        }
      }
      else
      {
        switch (seq[1])
        {
        case 'A':
          return kSpecialKeys::kArrowUp;
        case 'B':
          return kSpecialKeys::kArrowDown;
        case 'C':
          return kSpecialKeys::kArrowRight;
        case 'D':
          return kSpecialKeys::kArrowLeft;
        case 'H':
          return kSpecialKeys::kHome;
        case 'F':
          return kSpecialKeys::kEnd;
        }
      }
    }
    else if (seq[0] == 'O')
    {
      switch (seq[1])
      {
      case 'H':
        return kSpecialKeys::kHome;
      case 'F':
        return kSpecialKeys::kEnd;
      }
    }

    return kTerminalEsc;
  }
  else
  {
    return character;
  }
}

void Terminal::Die(const char *function_name)
{
  ClearScreen();

  perror(function_name);
  exit(1);
}

void Terminal::ClearScreen()
{
  Write(kTerminalClearScreen);
  Write(kTerminalCursorAtBegin);
}

void Terminal::DrawRows(const std::vector<std::string> &buffer)
{
  const std::string welcome{"EDISLAV editor- Bljat edition\r\n"};

  for (std::int32_t y{0}; y < static_cast<std::int32_t>(buffer.size()); ++y)
  {
    Write(buffer.at(y).c_str(), width_);
    Write("\r\n");
  }

  const int one_third_of_terminal{height_ / 3};
  for (std::int32_t y{static_cast<std::int32_t>(buffer.size())};
       y < height_ - 1; ++y)
  {
    if (buffer.size() == 0 && y == one_third_of_terminal)
    {
      Write(welcome);
    }
    else
    {
      Write("~\r\n");
    }
  }

  Write("~");
}

void Terminal::RefreshScreen(const std::vector<std::string> &buffer)
{
  // Turn-off cursore
  Write(kTerminalHideCursor);
  ClearScreen();

  DrawRows(buffer);

  std::string buff{kTerminalEscSequence + std::to_string(cursor_y_) + ";" +
                   std::to_string(cursor_x_) + "H"};
  Write(buff);

  // Turn-on cursore
  Write(kTerminalShowCursor);
}

void Terminal::GetWindowSize()
{
  struct winsize ws;

  if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0)
  {
    if (!Write(kTerminalCursorToRightColumn + kTerminalCursorToBottomRow))
    {
      Die("GetWindowSize");
    }
    GetCursorPosition();
  }
  else
  {
    width_ = ws.ws_col;
    height_ = ws.ws_row;
  }
}

void Terminal::GetCursorPosition()
{
  if (!Write(kTerminalQueryCursorPosition))
  {
    Die("GetCursorPosition");
  }

  std::array<char, 32> buff;
  std::uint32_t i{0};
  while (i < buff.size() - 1)
  {
    if (read(STDIN_FILENO, &buff.at(i), 1) != 1)
    {
      break;
    }
    if (buff.at(i) == 'R')
    {
      break;
    }

    ++i;
  }

  buff.at(i) = '\0';
  if (buff.at(0) != kTerminalEscSequence.at(0) ||
      buff.at(1) != kTerminalEscSequence.at(1))
  {
    Die("GetCursorPosition");
  }
  if (sscanf(&buff.at(2), "%d;%d", &height_, &width_) != 2)
  {
    Die("GetCursorPosition");
  }
}

bool Terminal::Write(const std::string &command)
{
  if (static_cast<std::size_t>(write(STDOUT_FILENO, command.c_str(),
                                     command.size())) != command.size())
  {
    return false;
  }
  else
  {
    return true;
  }
}

bool Terminal::Write(const std::string &command, std::size_t length)
{
  if (length > command.size())
  {
    length = command.size();
  }

  if (static_cast<std::size_t>(write(STDOUT_FILENO, command.c_str(), length)) !=
      length)
  {
    return false;
  }
  else
  {
    return true;
  }
}

void Terminal::MoveCursor(const std::int32_t character)
{
  switch (character)
  {
  case kSpecialKeys::kArrowLeft:
    if (cursor_x_ > 1)
    {
      --cursor_x_;
    }
    break;
  case kSpecialKeys::kArrowRight:
    if (cursor_x_ < width_)
    {
      ++cursor_x_;
    }
    break;
  case kSpecialKeys::kArrowUp:
    if (cursor_y_ > 1)
    {
      --cursor_y_;
    }
    break;
  case kSpecialKeys::kArrowDown:
    if (cursor_y_ < height_)
    {
      ++cursor_y_;
    }
    break;
  };
}

void Terminal::MoveCursorPageUpDown(const std::int32_t character)
{
  auto key = character == kSpecialKeys::kPageUp ? kSpecialKeys::kArrowUp
                                                : kSpecialKeys::kArrowDown;

  std::int32_t times =
      character == kSpecialKeys::kPageUp ? cursor_y_ : height_ - cursor_y_ + 1;

  while (--times)
    MoveCursor(key);
}

void Terminal::MoveCursorAtLineStart() { cursor_x_ = 1; }

void Terminal::MoveCursorAtLineEnd() { cursor_x_ = width_; }
