#ifndef EDISLAV_SRC_LIB_TERMINAL_H
#define EDISLAV_SRC_LIB_TERMINAL_H

#include <cstdint>
#include <string>
#include <termios.h>
#include <vector>

class Terminal
{
public:
  ~Terminal();
  static Terminal &GetInstance();
  std::int32_t ReadChar();
  void RefreshScreen(const std::vector<std::string> &buffer);
  void MoveCursor(const std::int32_t character);
  void MoveCursorPageUpDown(const std::int32_t character);
  void MoveCursorAtLineStart();
  void MoveCursorAtLineEnd();

private:
  Terminal();
  Terminal(const Terminal &);
  Terminal &operator=(const Terminal &);
  void EnableRawMode();
  void SaveTerminal();
  void RestoreTerminal();
  void Die(const char *function_name);
  void ClearScreen();
  void DrawRows(const std::vector<std::string> &buffer);
  void GetWindowSize();
  void GetCursorPosition();
  bool Write(const std::string &command);
  bool Write(const std::string &command, std::size_t length);

  struct termios original_termios_;
  struct termios raw_termios_;

  std::int32_t width_{0};
  std::int32_t height_{0};

  // Terminal cursor is one base addressable
  std::int32_t cursor_x_{1};
  std::int32_t cursor_y_{1};
};

#endif // EDISLAV_SRC_LIB_TERMINAL_H
