#ifndef EDISLAV_SRC_LIB_CHARACTER_H
#define EDISLAV_SRC_LIB_CHARACTER_H

#include <cstdint>

enum kSpecialKeys : std::int32_t
{
  kArrowLeft = 1000,
  kArrowRight,
  kArrowUp,
  kArrowDown,
  kPageUp,
  kPageDown,
  kHome,
  kEnd,
  kDelete
};

#endif // EDISLAV_SRC_LIB_CHARACTER_H
