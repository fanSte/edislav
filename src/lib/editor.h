#ifndef EDISLAV_SRC_LIB_EDITOR_H
#define EDISLAV_SRC_LIB_EDITOR_H

#include "src/lib/terminal.h"
#include <vector>

class Editor
{
public:
  Editor(std::int32_t argc, char *argv[]);
  void Run();
  void ParseArgs(std::int32_t argc, char *argv[]);

private:
  inline void EditorProccessKey(const std::int32_t character);
  void Open();

  Terminal &ui_ = Terminal::GetInstance();
  std::vector<std::string> buffer_;
  std::string file_name_;
};

#endif // EDISLAV_SRC_LIB_EDITOR_H
