#include "src/lib/editor.h"
#include "src/lib/character.h"
#include <fstream>
#include <sstream>

constexpr char CtrlKey(char character) { return character & 0x1F; }

void Editor::Run()
{
  while (true)
  {
    ui_.RefreshScreen(buffer_);
    EditorProccessKey(ui_.ReadChar());
  }
}

inline void Editor::EditorProccessKey(const std::int32_t character)
{
  switch (character)
  {
  case CtrlKey('q'):
    std::exit(0);
    break;

  case kSpecialKeys::kHome:
    ui_.MoveCursorAtLineStart();
    break;
  case kSpecialKeys::kEnd:
    ui_.MoveCursorAtLineEnd();
    break;

  case kSpecialKeys::kPageUp:
  case kSpecialKeys::kPageDown:
  {
    ui_.MoveCursorPageUpDown(character);
  }
  break;

  case kSpecialKeys::kArrowUp:
  case kSpecialKeys::kArrowDown:
  case kSpecialKeys::kArrowLeft:
  case kSpecialKeys::kArrowRight:
    ui_.MoveCursor(character);
    break;
  }
}

void Editor::Open()
{
  std::ifstream infile(file_name_);
  if (!infile.fail())
  {
    std::string line;
    std::int32_t index{0};
    while (std::getline(infile, line))
    {
      buffer_.push_back(line);
      ++index;
    }
  }
}

Editor::Editor(std::int32_t argc, char *argv[])
{
  ParseArgs(argc, argv);
  Open();
}

void Editor::ParseArgs(std::int32_t argc, char *argv[])
{
  if (argc > 1)
  {
    file_name_ = std::string{argv[1]};
  }
}
